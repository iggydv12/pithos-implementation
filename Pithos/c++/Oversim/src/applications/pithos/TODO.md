
# Pithos coding:

## Major:
---
- Implement object modification for group storage.
- Intuitivly, many more updates will occur, compared to puts, which strengthens the case for implementing update.
- Extend the pithos test application to add modify testing.
- Determine distributions of object requests and peer lifetimes to be used in PithosTestApp
- Distributions for put, get and update will probably vary greatly.
- These distributions have to be translated into their C/S counterparts, to enable profiling.
- Ensure sent get stats in Pithos test app, match the stats in Group storage. This can probably be fixed by recording the number of unhandled requests.
- Implement Pithos as a real world application
- Test Pithos on PlanetLab and EmuLab
- Develop grouping algorithm based on player traces
- Have Pithos construct groups based on the grouping algorithm
- Use player traces for node movement in Pithos

## Minor:
---
- Make the latitude and longitude ranges for peers and super peers a changeable parameter instead of 100
Support can be added for partially connected groups that will reduce network bandwidth at the cost of latency
Add a debug mode to the code, which ads the debugging code currently present in Pithos as an option and not always.
- The error condition of an unspecified peer should be logged.
- Group storage should record its own successes, failures and latencies and not depend on PithosTestApp from recording them indirectly.
- The DHT module can be changed so as to store and retrieve GameObjects instead of BinaryValues
A maximum distance needs to be defined for the directory server
- Ensure correctness for pithos under 32 bit systems. This includes testing long variable sizes and making them 32 bit safe.
