# Pithos, a distributed storage for massive multi-user virtual Environments, in action!
This is the official repository for the implementation of Pithos, a State Management and Persistency Architecture for Peer-to-Peer Massively Multi-user Virtual Environments

---

### From the future work, stated in dissertation
<details>
<summary>Click for details</summary>

* The next step in the Pithos implementation will be to add libraries to allow for
    physical network operation and then to deploy Pithos on a global scale network,
    such as PlanetLab. Although PlanetLab will not be able to show Pithos for
    larger numbers of nodes than one or two thousand, it will show real-world
    performance. It will also enable the measurement of resource requirements
    on a per-node basis, such as processor and memory requirements, that are
    quantities that cannot easily be measured in simulation.
* Before a real-world implementation, a “hardware in the loop” implementation
    of Pithos might be done. Oversim supports real-world computers to be connected
    to the simulated network. This will allow for the Pithos to be tested on
    a computer to determine memory and CPU requirements, while still having a
    larger network than what PlanetLab can provide.

</details>

---

### The first question that comes to mind is clearly, what is Pithos, how does it work, and what does it offer to the the industry?

<details>
<summary>Click for details</summary>

* The Pithos use case is that of a generic storage system, supporting storage, retrieval,
    modification and removal. Pithos is a hierarchical distributed storage system that
    uses grouping to reduce latencies of requests, as is required by massively multi-user
    virtual environments (MMVEs).
* Group storage, distance-based storage and replication are used to make Pithos
    responsive. Overlay storage, replication and repair are used to make Pithos reliable.  
    Certification, replication and quorum mechanisms are used to make Pithos secure.  
    Group storage and overlay storage are used to make Pithos fair.
* Users in Pithos are divided into groups which form group storage networks.
    Group storage networks are O(1) structured overlays that ensure one hop latencies
    for requests.  
* Super peers exist to manage groups and peer join groups using a directory
    server. Group ledgers are by each peer to keep track of the peers and objects
    stored within a group. A third-party O(log(N)) overlay storage implementation is
    used to allow nodes to retrieve data stored outside of their groups.
* Pithos is implemented as an Oversim simulation that runs on the OMNeT++
    network simulation framework. The simulation allowed for the evaluation of Pithosusing up to 10,400 nodes, using realistic latency profiles.

</details>

---

### How was Pithos measured?

<details>
<summary>Click for details</summary>

* The responsiveness and reliability of Pithos was evaluated, taking bandwidth usage
into account. Pithos performance was compared to overlay storage performance. 

* What was found in Pithos is that a trade-off exists between reliability and bandwidth.
Pithos can be made highly reliable by increasing the amount of bandwidth
used and reliability decreases with a decrease in bandwidth.Pithos is more reliable
than overlay storage for the same amount of bandwidth used. Pithos uses a total
of 1950 Bps bandwidth to achieve 99.98% reliability, while the most reliable overlay
storage configuration tested achieved 93.65% reliability, using 2182 Bps bandwidth.

* Pithos is also more responsive than overlay storage, with a responsiveness of 0.192s,
compared with the overlay responsiveness of 1.4s.
A latency of 192ms for Pithos might seem high, but the effect of the underlay
network should be taken into account. This was shown by simulating Pithos in a
LAN environment, where it achieved a responsiveness of 1.6ms in a network where
each hop had a 1ms delay.  

* When comparing group probabilities, it was found that the worst Pithos could
perform was equal to the performance of overlay storage.
The performance of Pithos under invalid data attacks were also investigated for
various probabilities of malicious nodes. It was found that Pithos performs consistently
better than overlay storage i.t.o. reliability. As expected, using a quorum
mechanism further improved the Pithos reliability. At 40% malicious users, overlay
storage is 46% reliable. In comparison, when using the least bandwidth, Pithos is
60% reliable whilst it is 96% reliable when using the most bandwidth.
The distribution of objects on peers were also investigated to determine the
fairness of Pithos. It was found that Pithos is comparable i.t.o. fairness to overlay
storage.  

* The scalability of Pithos was also tested, by quadrupling the network size, increasing
the number of nodes in the simulation by 300% from 2600 nodes to 10,400
nodes. The overall reliability and responsiveness remained the same at 99.7% and
0.19s respectively. The bandwidth usage, however, increased by 20% from 1375 Bps
to 1652 Bps due to increased bandwidth usage by overlay storage.
It was, therefore, found that Pithos is a reliable, responsive, fair and scalable
state management and persistency architecture that will be able to fulfill its role as
the authoritative object store for the P2P MMVEs of the future.
Simulating for 10,400 nodes is also 10 times larger than the P2P Second Life
Walkad simulation of 1024 nodes. 

* Before Pithos, Walkad is considered to be
the most recent and significant contribution to the area of state management and
state persistency for P2P MMVEs. The advantage of Walkad is that it is a practical
implementation of a state management and persistency architecture that is already
integrated into an existing MMVE. The disadvantages ofWalkad are that it does not
take security consideration into account; it runs on an existing file sharing network
where users may have different usage profiles; it was emulated on a relatively small
scale (1024 peers and 600 objects at most); and its theoretical performance is below
that of Pithos. The Walkad routing time for local queries is between O(1) and
O(Nc) depending on the cell distribution, where Pithos’s routing time is fixed at
O(1).

</details>

---

### How can we implement Pithos on a real-world system?

<details>
<summary>Click for details</summary>

Simply put, the goal of this project is to implement this state management and consistency architecture (SMSCA), on a real world system, running real traffic and user interaction. After the model as been implemented, measurements will need to be taken, which we will then use to compare against the simulated results achieved in the dissertation.

The first observation that can be made is that the project is written in C++, and built in Oversim/Omnet++ as a large scale network simulation.
This means that in order for us to implement this State management & consistency architecture, we'll need to create an API, in the language of our choice, that can be used to implement the Pithos architecture in a real world MMVe. 
In order to realise this goal, we can break this implementation into smaller chunks, to help identify possible stumbling blocks and design flaws.  
The following steps should be taken:
 - Understand current API & logic and simplify as far as possible and design a simple new API
 - Create openAPI specification for new API
 - Implement new API in Java/Kotlin
 - Basic API testing
 - Create small skeleton network, that can be used to test new API with an "real" client
 - Add more "real" clients to the test network (this is where things could start to get more tricky)
 - Design Full model and implement full API & Pithos logic
 - Run on skeleton network
 - Define measurement criteria
 - Implement simple measurements on Skeleton network
 - Create a MMVe (minecraft server) and implement Pithos as SMSCA 
 - Measure results 
 - Compare with simulated results

</details>

---

### Overlay Storage
<details>
<summary>Click for details</summary>

Seeing as a lot of the actual Pithos architecture is implemented in oversim, like the overlay storage (underlay storage), will a solution for this need to also be developed?
Luckily the above statement is not true, in the case of this project, an existing overlay network should be used, something like Pastry, Chord or Gia, but more research should be done to find a suitable implementation

</details>

---

### Assumptions

<details>
<summary>Click for details</summary>

For design simplification purposes, we can make the following assumptions
- Overlay Storage is provided (Pastry, Chord, Gia etc)
- A generic interface of hooking the new API/service to a MC server exists (Spigot)
- The ability to emulate large networks for testing exists (mininet)
- ...

</details>

---

### Possible implementation strategies

There are (for now) two ways in which this project can be implemented

<details>
<summary>Click for details</summary>

1. Write a Mod/component for Spigot which uses the Pithos java API that contains the api layer, storage layer, comms layer etc
2. Create an adapter that sends storage commands etc to a DB (SQL) client, more information is required 

Regardless of which approach is taken, a few steps are required to have the Pithos architecture implemented on a real world system like Minecraft. To allow this, we need an interface between the two systems to allow communication. The following are some important design points

- The design and implementation of a simple storage API
- Some kind of grouping algorithm
- Some kind of super-peer selection algorithm

</details>

---

### Bootstrapping mechanism

![Bootstrap](/Pithos/pictures/bootstrap.jpg "Logo Title Text 1")

### DirectoryLogic

<details>
<summary>Code snippet</summary>

```C++
/**
* Called when the module is being created
*
* @param stage Which stage of initialisation Oversim is in.
*/
void initializeApp(int stage);

/** Called when the module is about to be destroyed */
void finishApp();

/**
* Called when a timer message is received
* @param msg The timer message
*/
void handleTimerEvent(cMessage* msg);

/**
* Called when a message is received from the overlay
*
* @param key The routing key of the received overlay message
* @param msg The message received from the overlay
*/
void deliver(OverlayKey& key, cMessage* msg);

/**
* Called when a UDP message is received
*
* @param msg The received UDP message
*/
void handleUDPMessage(cMessage* msg);

/**
* Function called when a peer requests to join the network.
* The function replies with a Super Peer address which the
* peer should contact.
*
* @param boot_req Message containing the joining peer's location and address information.
*/
void handleJoinReq(bootstrapPkt *boot_req);

/**
* Function that calculates the nearest super peer to the joining peer.
*
* @param lati Latitude of the joining peer.
* @param longi Longitude of the joining peer.
*
* @return The TransportAddress (IP and port) of the nearest super peer
*/
TransportAddress findAddress(const double &lati, const double &longi);

/**
* Handles a request by a super peer to be added to the directory.
*
* @param boot_req The address and location of the super peer that wishes to be added to the directory.
*/
void handleSuperPeerAdd(bootstrapPkt *boot_req);

/**
* Checks whether any super seers have been added to tDirectory Server
*
* @return True if there are super seers, and false otherwise.
*/
bool superPeersExist();
```
</details>

---

### Leave mechanism

![alt text](/Pithos/pictures/leave.jpg "API")
<details>
<summary>Click for details [pages 85-86] </summary>

1. At regular intervals, each group peer uniformly randomly selects another peer
in the group to send a keep-alive (Ping) message to.

2. If a peer received a keep-alive message, it responds with an acknowledge (Pong)
message.

3. A peer that does not respond with an acknowledge message within a sufficiently
large amount of time is considered to have left the network.

4. The originating peer of the keep alive message informs the super peer that the
target of the keep-alive message is thought to have left the group.

5. The super peer then also sends a keep-alive message to the target peer, to
ensure that there does not exist some communications issue between the two
group peers, and that the target peer has actually left the network.

6. If the target peer does not respond to the super peer, the super peer informs
all group peers that the target peer has left the group.
</details>

---

### Store Object

![alt text](/Pithos/pictures/store.jpg "API")
<details>
<summary>Click for details [pages 91-92] </summary>

* Overlay storage implements an existing DHT

* The number of nodes selected from the group ledger for storage relates to the number of required object replicas

* Nodes are selected in a uniformly random

* If a node is selected
more than once, it reduces the effective number of replicas in the network, which
reduces the expected object lifetime

* Two types of storage mechanisms: _Fast_ & _Safe_
    * Fast - wait only for first success or fail
    * Safe - wait for all success or fail (Quorum)
</details>

---

### Retrieve Object
![alt text](/Pithos/pictures/retrieve.jpg "API")
<details>
<summary>Click for details [pages 93-94] </summary>

* Pithos first checks whether the object that was requested exists within the
group by checking its group ledger. This is required, because the higher layer
might have requested an object that is stored in another group.

* Out-of-group requests are only serviced by overlay storage

* If the object is housed in the
Pithos peer’s local store, the object is sent to the higher layer. If the object
is not housed locally, Pithos requests the object from both overlay and group
storage

* The group ledger is used to determine which group peers contain which objects. The number of peers selected depends on the retrieval type.

* Three types of retrieval mechanisms: _Fast_, _Parallel_ & _Safe_
    * Fast - wait only for first retrieval response, only one peer requested
    * Parallel - wait only for first retrieval response, multiple peers requested
    * Safe - wait for all retrieval response, from many peers (Quorum)
</details>

---

### Modify Object

<details>
<summary>Similar to Store - Click for details [pages 95] </summary>

* The overlay
handles the modify request and responds with success or failure. Group storage
received the modify request and checks the group ledger whether the object exists
within the group. If the object does not exist in the group, it is only modified in the
overlay

* To keep track of modified objects, each object has a version number that is
incremented every time an object is modified. This allows retrieve requests to select
the object with the latest version number to be sent to the higher layer

* If the object exists within the group, all peers that contain the object are identified
from the group ledger and modify requests are sent to them

* The modify
request is sent to the destination peer, where the object in the local store is updated
and its version number incremented

* If a retrieve request receives objects with multiple
version numbers, it selects the set of objects with the latest version number for
“safe” comparison
</details>

---

### Remove Object

<details>
<summary>Click for details [pages 95] </summary>

* Removal is handled by the object TTL. 

* It is not possible to define an explicit remove
operation for overlay storage, since a peer may be off-line when the removal request
is sent. If the peer rejoins the network, the object becomes available again. 

* Object
TTL ensures that the storage space requirements does not increase indefinitely
during the lifetime of the storage system.
</details>

---

### Repair Object
![alt text](/Pithos/pictures/repair.jpg "API")
<details>
<summary>Click for details [pages 90] </summary>

* Two types of repair mechanisms were implemented:
periodic repair and leaving repair.

* With periodic repair, the super peer periodically checks the number of available
replicas of every object in its group ledger.

* Because the super peer itself contains no
objects, it requests that an object be replicated from a peer that does contain the
object.

* That peer will receive a repair request for a specific object, select a group
peer in a uniformly random fashion, that does not already contain the object, and
initiate a store request to that peer.

* With leaving repair,
the super peer repairs all objects of a peer that leaves the group.

* Requests can fail due to peers leaving the network as they receive a store request


</details>

---

### Basic Peer API Overview

Peer Node:

![alt text](/Pithos/pictures/overview.jpg "API")

<details>
<summary>Click for details (From original API spec)</summary>

_Pithos API Definition_
---

This document contains an API specification for Pithos, a Distributed Storage Architecture for Massive Multi-User Virtual Environments. 
Pithos is designed to handle both state management and state persistency, and its
novelty is the group-based distance-based approach that supports both a responsive
and a load-balanced storage system. The purpose of Pithos is to allow for efficient object
storage and retrieval, and therefore Pithos can perform the following four operations:

- Store object
- Retrieve object
- Modify object
- Remove object

These operations are achieved by making use of the following mechanisms:

- Group based, Distance based storage
- Overlay Storage: inter-group storage (out-of-group storage)
- Replication
- Repair
- Certification
- Quorum

Which help achieve the following criteria for a State management / persistency architecture:

- Responsiveness
- Reliability
- Fairness
- Security


**Store object**
---

The VE logic will store data when a new object is added to the VE
state. This can happen as a consequence of an event that leads to the generation of
a new object.

Refer to Chapter 4.4.1 - A State Management and Persistency Architecture for
Peer-to-Peer Massively Multi-user Virtual Environments

**Retrieve object**
---

Object retrieval will be required every time an event is received.
The VE logic will retrieve the object state from memory, which is part of state management.
The state of other objects might also be retrieved to determine the effect of
the event that was received.

Refer to Chapter 4.4.2 - A State Management and Persistency Architecture for
Peer-to-Peer Massively Multi-user Virtual Environments

**Modify object**
---

Object modification occurs every time an object update is generated,
since it requires a modification of the object state.

Refer to Chapter 4.4.3 - A State Management and Persistency Architecture for
Peer-to-Peer Massively Multi-user Virtual Environments

**Remove object**
---

Object removal might be required to save storage space, although
this is not essential to the correct functioning of the storage system. Without object
removal, it is necessary for Pithos to keep a record of the most recent version of an
object, so stale object states are not returned by the object retrieval operation.
Removal is handled by the object TTL. It is not possible to define an explicit remove
operation for overlay storage, since a peer may be off-line when the removal request
is sent. If the peer rejoins the network, the object becomes available again. Object
TTL ensures that the storage space requirements does not increase indefinitely
during the lifetime of the storage system.
Refer to Chapter 4.4.3 - A State Management and Persistency Architecture for
Peer-to-Peer Massively Multi-user Virtual Environments

</details>

### PeerLogic

<details>
<summary>Code snippet</summary>

```C++
/**
* Handle a request from the higher layer for store
*
* @param capiPutMsg the request containing the GameObject
*/
void handlePutCAPIRequest(RootObjectPutCAPICall* capiPutMsg);

/**
* Handle a request from the higher layer for retrieve
*
* @param capiGetMsg the request containing the OverlayKey
*/
void handleGetCAPIRequest(RootObjectGetCAPICall* capiGetMsg);
```

### PeerData

```C++
/**
* Set the TransportAddress
*
* @param adr A TransportAddress object containing the IP and port information
*/
void setAddress(TransportAddress adr);

/**
* Add the TransportAddress
*
* @param ip_str A string specifying the IP of the location
* @param port The port of the object location
*/
void setAddress(const char* ip_str, int port);

TransportAddress getAddress();
```

### PeerLedger
```C++
void addObjectRef(ObjectDataPtr object_data_ptr);

ObjectDataPtr getObjectRef(const int &i);

unsigned int getObjectListSize();

bool isObjectPresent(ObjectDataPtr object_ptr);

void eraseObjectRef(const int &i);

void eraseObjectRef(ObjectDataPtr object_data_ptr);

//Record the number of objects to be able to calculate an average at the end of life. This should be done at regular intervals, otherwise the average is incorrect.
void recordObjectNum();
double getObjectAverage();
```
</details>

### PeerListPk
<details>
<summary>Code snippet</summary>

```C++

/**
 * Set the size of the peer_list vector
 *
 * @param size The number of elements that the vector should be able to contain
 */

virtual void setPeer_listArraySize(unsigned int size) {}

/**
 * Get the size of the peer_list vector
 *
 * @returns size The number of elements that the vector contains
 */

virtual unsigned int getPeer_listArraySize() const {return peer_list.size();}

/**
* Retrieve a single peer's information from the list of peers
*
* @param k the position of the peer in the list
* @returns The peer information at the specified position
*/
virtual PeerData& getPeer_list(unsigned int k) {return peer_list.at(k);}

/**
* Set the peer information for a particular element in the list
*
* @param k the position of the element
* @param alist the peer information to be inserted
*/
virtual void setPeer_list(unsigned int k, const PeerData& alist) {peer_list.at(k)=alist;}

/**
* Add a single peer information item to the list vector
*
* @param alist the peer information to be added
*/
virtual void addToPeerList(const PeerData& alist) {peer_list.push_back(alist);}

/**
* Delete all elements contained in the peer list
*/
virtual void clearPeerList() {peer_list.clear();}
```
</details>
