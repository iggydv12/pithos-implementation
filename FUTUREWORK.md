Scope:
----------------------------
Here the scope of the project will be defined. 

Project Specification:
----------------------------

[Pithos future work]
* evaluating Pithos using a realistic model of avatar movement within the VE
* develop of a distributed grouping algorithm that is able to dynamically alter group membership to accommodate users aggregating to popular locations in the VE
* effect of group size on storage should be investigated.
* The use of secondary peers should be implemented to improve the robustness of
* Pithos in the event that a super peer unexpectedly disconnects from the network.
* implementation of a distributed super peer selection algorithm to ensure that the ratio of super peers to peers results in responsive and reliable storage.
* investigate replacing overlay storage with Voronoi-based storage architecture, such as VoroStore or Hivory.
* evaluate Pithos using a real MMVE, Koekepan research platform
